function PrototypeService(Restangular) {

	class PrototypeService {
		constructor(endpoint) {
			this.endpoint = endpoint;
			let service = Restangular.service(endpoint);
			Object.assign(this,service);
			this.Restangular = Restangular;

		}
	}

	return PrototypeService;
}




export default ['Restangular',PrototypeService];
