function ArtistModel(Restangular, $q, PrototypeModel) {

	var endpoint = 'artists';
	class Artist extends PrototypeModel {
		constructor(object = {}) {
			let defaults  = {
				id : null,
				external_urls : '',
				followers : {},
				genres : [],
				href : '',
				images : [],
				name : '',
				someCustomProperty : 'foo',
				popularity : null,
				type : '',
				uri : '',
			};
			super(endpoint,defaults,object);
			//super.extendModel(object);
		}

		getFullName() {
			return this.firstName + " " + this.lastName;
		}


		_requestInterceptor(element, operation, what) {

			console.log(` on element: ${what} , operation: ${operation}`);
			if (what == endpoint) {
				if (operation === "put" || operation === 'post') {

				}
			}
			return element;
		}

	}

	return Artist;
}

export default ['Restangular', '$q', 'PrototypeModel', ArtistModel];

