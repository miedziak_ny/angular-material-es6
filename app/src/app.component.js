import AppController from './app.controller';

export default {
    name : 'appComponent',
    config : {
        templateUrl      : 'src/app.view.html',
        controller       : ['$mdSidenav','$scope','$log', AppController],
        controllerAs     : '$app'
    }
};