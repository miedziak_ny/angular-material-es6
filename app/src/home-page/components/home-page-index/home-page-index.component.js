import HomePageIndexController from './home-page-index.controller'
export default {
    name: 'homePageIndex',
    config: {
        bindings: {},
        templateUrl: 'src/home-page/components/home-page-index/home-page-index.view.html',
        controller: ['HomePageIndexService','ArtistsService','ArtistModel', HomePageIndexController]
    }
};
