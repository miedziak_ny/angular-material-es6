class AppController {
    constructor($mdSidenav,$scope,$log) {
        var self = this;
        self.toggleSlideNav = toggleSlideNav;

        function toggleSlideNav() {
            $mdSidenav('left').toggle();
        }
    }

}

export default AppController;
