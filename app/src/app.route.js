export default function($locationProvider, $stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/index/home");

    $stateProvider
        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "./src/app.template.html",
        })
        .state('index.home', {

            url: "/home",
            views : {
                content : {
                    component: "homePageIndex"
                },
            },

        })




}