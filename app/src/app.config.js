
function restangularConfig(RestangularProvider) {
	RestangularProvider.setBaseUrl('https://api.spotify.com/v1/');
	RestangularProvider.setErrorInterceptor(function (response, deferred, responseHandler) {
        console.error('error',response);
    });
}
function modelConfig(ArtistModel) {
	 new ArtistModel().extendRestangularModel().setRestangularInterceptor();

}

function mdConfig($mdIconProvider,$mdThemingProvider) {
    $mdIconProvider
        .defaultIconSet("./assets/svg/avatars.svg", 128)
        .icon("menu", "./assets/svg/menu.svg", 24)
        .icon("share", "./assets/svg/share.svg", 24)
        .icon("google_plus", "./assets/svg/google_plus.svg", 24)
        .icon("hangouts", "./assets/svg/hangouts.svg", 24)
        .icon("twitter", "./assets/svg/twitter.svg", 24)
        .icon("phone", "./assets/svg/phone.svg", 24);

    $mdThemingProvider.theme('default')
        .primaryPalette('teal')
        .accentPalette('orange');
}


export {restangularConfig,modelConfig,mdConfig};